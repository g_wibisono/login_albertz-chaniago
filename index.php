<!DOCTYPE HTML>
<?php session_start(); 
if(!empty($_SESSION[ 'login_user']))
{ 
	header( 'Location: utama.php'); exit();
} 
?>
  <html lang="en">
    
    <head>
      <meta charset="UTF-8" />
      <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0">
      <title>
        My Site
      </title>
      <link rel="stylesheet" href="css/bootstrap.css"/>
      <link rel="stylesheet" href="css/custom.css"/>
      
    </head>
    
    <body>
      <div class="container">
        <div id="box" class="">
          <div class="card card-container wella">
            <img id="profile-img" class="profile-img-card" src="images/ava.jpeg" />
            <p id="profile-name" class="profile-name-card">
            </p>
            <form action="" method="post">
              <label>
                Username
              </label>
              <input type="text" name="username" class="form-control" autocomplete="off" id="username" required/>
              <label>
                Password
              </label>
              <input type="password" name="password" class="form-control" autocomplete="off" id="password" required/>
              <br/>
              <input type="submit" class="btn btn-lg btn-primary btn-block btn-signin" value="Login" id="login"/>
              <span class='alert'>
              </span>
              <div id="error">
              </div>
            </form>
          </div>
        </div>
      </div>
	  <script src="js/jquery.min.js"></script>
      <script src="js/jquery.ui.shake.js"></script>
      <script>
        $(document).ready(function() {
          $('#login').click(function() {
            var username = $("#username").val();
            var password = $("#password").val();
            var dataString = 'username=' + username + '&password=' + password;
            if ($.trim(username).length >= 0 && $.trim(password).length >= 0) {
              $.ajax({
                type: "POST",
                url: "terawang.php",
                data: dataString,
                cache: false,
                beforeSend: function() {
                  $("#login").val('Loading...');
                },
                success: function(data) {
                  if (data=='OK') {
                    $("body").load("utama.php").hide().fadeIn(1500).delay(6000);
                  } 
				  else {
                    $('#box').shake();
                    $("#login").val('Login')
                    $("#error").html("<span style='color:#EB242E'>Warning!!</span> username dan password salah. ");
                  }
                }
              });
            }
            return false;
          });
        });
      </script>
      <script src="js/jquery.backstretch.js"></script>
      <script>
        $.backstretch(["images/1.jpg", "images/2.jpg", "images/3.jpg", "images/4.jpg"], {
          fade: 750,
          duration: 4000
        });
      </script>
    </body>
  
  </html>